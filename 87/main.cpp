#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

#include <set>

using namespace std;

vector<int> find_primes(int end) {

    vector<int> values(end);

    iota(values.begin(), values.end(), 1);

    //mark all entries that are evenly divisible 
    //by a previous value by setting them to 1
    //could be optimized further
    for(int i = 0; i < values.size(); i++) {

        if(values[i] != 1) {

            for(int k = 2*i+1; k < values.size(); k += values[i]) {

                values[k] = 1;

            }

        }

    }

	values.erase(remove(values.begin(), values.end(), 1), values.end());

    return values;

}

int main(int argc, char *argv[]) {

	int limit = 50000000;

	cout << "calculating primes up to 50,000,000..." << endl;

	vector<int> primes = find_primes(limit);

	set<int> discovered;

	cout << primes.size() << " primes found" << endl;

	int count = 0;
	
	int val, square, cube, fourth;

	for(int i = 0; i < primes.size(); ++i) {

		fourth = primes[i]*primes[i]*primes[i]*primes[i];

		if(fourth >= limit) {
			break;
		}

		for(int k = 0; k < primes.size(); ++k) {

			cube = primes[k]*primes[k]*primes[k];

			if(fourth+cube >= limit) {
				break;
			}

			for(int m = 0; m < primes.size(); ++m) {
				
				square = primes[m]*primes[m];

				val = square + cube + fourth;

				if(val < limit && discovered.find(val) == discovered.end()) {
					//cout << primes[m] << "^2 + " << primes[k] << "^3 + " << primes[i] << "^4 = " << val << endl;
					discovered.insert(val);
					count++;
				} 

				if(val >= limit) {
					break;
				}

			}
		}
	}

	cout << count << " valid prime power triples found" << endl;

}
