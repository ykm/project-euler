#include <iostream>
#include <cmath>
#include <set>
#include <string>

using namespace std;

//correctly finds the answer: 932718654 
//in 3m43.957s, without optimization flags, 
//in 0m27.980s, with -O3
//could be better optimized if a few functions 
//were written to handle substring operations 
//on ints, removing the need to convert from int -> string,
//copy string, string -> int, and back, etc

//returns the number of digits in a number
int num_digits(int n) {

	int count = 0;

	while(n > 0) {

		count++;
		n = n/10;

	}

	return count;

}

int concatenated_product(int n, int range) {

	int ret = 0;

	for(int i = 1; i <= range; ++i) {

		int val = n * i;

		ret = ret*pow(10, num_digits(val)) + val;

	}

	return ret;

}

bool is_pandigital(int n) {

	set<int> pandigits = {1,2,3,4,5,6,7,8,9};
	set<int> actual; 

	int digit;

	while(n > 0) {

		digit = n%10;

		actual.insert(digit);

		n = n/10;

	}

	return pandigits == actual;

}

int largest_pandigital(int min, int max) {

	for(int prospect = max; prospect >= min; --prospect) {

		if(is_pandigital(prospect)) {

			cout << "checking: " << prospect << endl;

			//string conversions for substring convenience
			string sp = to_string(prospect);

			//the first segment of the concatenation is nx1,
			//iteratively pick the first i digits and check if the 
			//next digits match nx2, nx3 ...
			for(int i = 1; i < sp.size(); ++i) {

				string sp_first = sp.substr(0, i); 
				
//				cout << "SP FIRST: " << sp_first << endl;

				string sp_remain = sp;
				sp_remain.erase(0, i);

				int n = 2;

				while(true) {

					//if size == 0, all substrings matched
					//and the prospective value is correct
					if(sp_remain.size() == 0) {
						return prospect;
					}

					int product = stoi(sp_first) * n;
					string spro = to_string(product);

/*
					cout << "\tREMAIN: \"" << sp_remain << "\"" << endl;
					cout << "\tPRODUCT: \"" << spro << "\"" << endl;
					cout << "\t" << sp_remain.find(spro) << endl;
*/

					//if the product is found at the beginning, 
					//remove and continue
					if(sp_remain.find(spro) == 0) {
						sp_remain.erase(0, spro.size());
						n++;
					} else {
						break;
					}

				}
				

			}

		}

	}

	return -1;

}

int main(int argc, char *argv[]) {

	//the largest possible 1-9 pandigital is 987654321
	//918273645 is given as an example, so the solution is between the two
	int solution = largest_pandigital(918273645, 987654321);

	cout << "solution found : " << solution << endl;

	return 0;

}
