#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <set>
#include <vector>
#include <climits>

using namespace std;

//could be more efficient, 
//specifically could use c double array instead of vectors,
//and parsing could be improved

void print_table(vector<vector<int> > table) {

	for(int i = 0; i < table.size(); ++i) {

		for(int k = 0; k < table[i].size(); ++k) {

			cout << table[i][k] << ", ";

		}

		cout << endl;

	}

}

vector<vector<int> > read_table(char *filename) {

	vector<vector<int> > table;
	
	ifstream file;

	file.open(filename);

	if(!file.is_open()) {
		cout << "invalid filename" << endl;
		return table;
	}

	string line;

	while(getline(file, line)) {

		vector<int> row;

		istringstream ss(line);
		string cell;
		
		while(getline(ss, cell, ',')) {

			//if cell represents a non connection, use placeholder value
			if(cell == "-") {
			
				row.push_back(0);

			} else {
			
				row.push_back(stoi(cell));
			
			}

		}

		table.push_back(row);

	}

	return table;

}

int sum_of_all_connections(vector<vector<int> > table) {

	int sum = 0;

	//could try iterators
	for(int i = 0; i < table.size(); ++i) {

		//start at i, as the table is mirrored across i=k
		for(int k = i; k < table[i].size(); ++k) {

			sum += table[i][k];

		}

	}

	return sum;

}

//determines the total weight of a minimum spanning tree
int mst_total_weight(vector<vector<int> > table) {

	set<int> visited;
	set<int> unvisited; 

	int total_weight = 0;

	//initialize unvisited 
	for(int i = 0; i < table.size(); ++i) {
		unvisited.insert(i);
	}

	//choose the first node as the starting point
	visited.insert(0);
	unvisited.erase(0);

	//assumes that the table represents a continuous graph
	while(unvisited.size() > 0) {

		//find the next lowest weight node to add
		int weight = INT_MAX;
		int node = -1;

		for(set<int>::iterator it = visited.begin(); it != visited.end(); ++it) {
			
			for(int k = 0; k < table[*it].size(); ++k) {

				//if the cell isn't a placeholder, the node k hasn't already been visited, and
				//the weight is less than the current minimum, save edge
				if(table[*it][k] != 0 && visited.find(k) == visited.end() && table[*it][k] < weight) {
					
					weight = table[*it][k];
					node = k;

				}

			}

		}

		//visit the node
		visited.insert(node);
		unvisited.erase(node);
		total_weight += weight;

	}
	
	return total_weight;

}

int main(int argc, char *argv[]) {

	if(argc < 2) {
		cout << "need filename" << endl;
		return -1;
	}

	vector<vector<int> > table = read_table(argv[1]);

	if(table.size() == 0) {
		return -2;
	}

	int original_weight = sum_of_all_connections(table);

	int new_weight = mst_total_weight(table);

	cout << "original weight: " << original_weight << endl;

	cout << "new weight: " << new_weight << endl;

	cout << "savings: " << original_weight-new_weight << endl;

	return 0;

}
