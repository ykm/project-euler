#include <iostream>

using namespace std;

unsigned int sum_of_evens(int max) {

	int n1 = 1;
	int n2 = 2;

	unsigned int sum = 0;

	while(n2 <= max) {

		//if the current value is even, add
		if(n2 % 2 == 0) {
			sum += n2;
		}

		//update fibonacci values
		n2 = n1 + n2;
		n1 = n2 - n1;

	}

	return sum;

}

int main(int argc, char *argv[]) {

	cout << sum_of_evens(4000000) << endl;

}
