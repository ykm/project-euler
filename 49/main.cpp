#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <set>

using namespace std;

vector<int> find_primes(int end) {

	vector<int> values(end); 

	iota(values.begin(), values.end(), 1);

	//mark all entries that are evenly divisible 
	//by a previous value by setting them to 1
	//could be optimized further
	for(int i = 0; i < values.size(); i++) {

		if(values[i] != 1) {

			for(int k = 2*i+1; k < values.size(); k += values[i]) {
			
				values[k] = 1;
				
			}

		}

	}

	//don't remove marked elements, 
	//makes finding the sequence more difficult

	//remove all marked elements
//	values.erase(remove(values.begin(), values.end(), 1), values.end());

	return values;

}

//checks if a is a permutation of b
bool is_permutation(int a, int b) {

	string sa = to_string(a);
	string sb = to_string(b);

	if(sa.size() != sb.size()) {
		return false;
	}

	set<char> set_a;

	for(char c: sa) {
		set_a.insert(c);
	}


	set<char> set_b;
		
	for(char c: sb) {
		set_b.insert(c);
	}

	return set_a == set_b;

}

//finds the first 3 element sequence in a list of primes that fulfils
//the following criteria:
//	all 3 elements are permutations of each other
//  the increase from the first to second, and second to third values are equal
//	the first element isn't 1487
vector<int> find_sequence(vector<int> primes) {

	int first = 0;
	int second = 0;
	int third = 0;

	for(int i = 0; i < primes.size(); i++) {

		if(primes[i] != 1 && primes[i] != 1487) {

			//choose first
			first = primes[i];

			//choose candidate for third, and check if a 
			//second is evenly between them
			for(int k = i+2; k < primes.size(); k++) {

				if(is_permutation(first, primes[k])) {

					third = primes[k]; 

					//check if candidate between first and third 
					//is prime, and is a permutation 
					int difference = (third - first)/2;

					second = first + difference;

					//check if second is prime and a permutation
					//index is value-1

					if(primes[second-1] != 1 && is_permutation(first, second)) {

						return {first, second, third};

					}

				}

			}

		}


	}

	return {-1, -1, -1};

}

int main(int argc, char *argv[]) {

	cout << "finding primes... ";
	vector<int> primes = find_primes(10000);
	cout << "OK" << endl;

	cout << "searching for prime permutation (other than 1487, 4817, 8147)...";
	vector<int> sequence = find_sequence(primes);
	cout << "OK" << endl;

	cout << endl;
	cout << "found: " << sequence[0] << ", " << sequence[1] << ", " << sequence[2] << endl;


}
