#include <iostream>

using namespace std;

unsigned int sum_of_multiples(int num1, int num2, int max) {
	
	unsigned int sum = 0;

	for(int i = 0; i < max; ++i) {

		if(i % num1 == 0 || i % num2 == 0) {
		
			sum += i; 
			
		}

	}
	
	return sum;

}

int main(int argc, char *argv[]) {

	cout << sum_of_multiples(3, 5, 1000) << endl;;	

}
